import React, { Component } from 'react';
import Comment from './component';
import { PropTypes } from 'prop-types';

class CommentContainer extends Component {

    render() {
        const { comment } = this.props;

        return <Comment comment={comment}/>;
    }
}

CommentContainer.propTypes = {
    comment: PropTypes.object.isRequired,
};