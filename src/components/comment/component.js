import React from 'react';
import { withStyles } from '@material-ui/core/styles/index';
import { PropTypes } from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import { RecipeRate } from '../recipe-rate';

const styles = {
    textArea: {
        width: '50%',
        height: 120,
        display: 'block',
    },
    commentHeader: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    icon: {
        fontSize: 40,
    }
};

const Comment = (props) => {
    const { comment: { date, reviewer, rate, likes, text }, classes: { commentHeader, icon } } = props;
    const dateToRender = new Date(date);
    return (
        <div>
            <div className={commentHeader}>
                <Avatar alt="reviewer">
                    <AccountCircleIcon className={icon}/>
                </Avatar>
                <span>Reviewer: {reviewer.name}</span>
                <span>Date: {dateToRender.toLocaleDateString()}</span>
                <RecipeRate rate={rate} iconSize={18}/>
            </div>
            <p>{text}</p>
            <div>
                <ThumbUpIcon/>
                <span>{likes}</span>
            </div>
        </div>
    );
};

Comment.propTypes = {
    classes: PropTypes.object.isRequired,
    comment: PropTypes.object.isRequired,
};

export default withStyles(styles)(Comment);