import React, { Component } from 'react';
import ReviewsComments from './component';
import { PropTypes } from 'prop-types';
import { saveComment } from '../../actions';
import { connect } from 'react-redux';

class ReviewsCommentsContainer extends Component {

    saveComment = (comment) => () => {
        const { saveComment } = this.props;
        saveComment(comment);
    };

    render() {

        return <ReviewsComments comments={this.props.comments} onAddCommentClick={this.saveComment}/>
    }
}

ReviewsCommentsContainer.propTypes = {
    comments: PropTypes.array.isRequired,
    saveComment: PropTypes.func.isRequired,
    recipe: PropTypes.object.isRequired,
};

export default connect(null, { saveComment })(ReviewsCommentsContainer);