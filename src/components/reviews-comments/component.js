import React from 'react';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import { PropTypes } from 'prop-types';
import Comment from '../comment/component';
import { RecipeRate } from '../recipe-rate';

const styles = {
    textArea: {
        height: 120,
        display: 'block',
        resize: 'none',
        border: 'none',
        width: '100%',
        boxSizing: 'border-box',
        padding: '10px 20px',
        outline: 'none',
        fontFamily: 'inherit',
        fontSize: 15,
    },
    commentsContainer: {
        border: '1px solid grey',
        width: '70%',
        margin: '0 auto',
    },
    saveComment: {
        width: '100%',
        borderRadius: 0,
    },
    rateContainer: {
        padding: '10px 0 0 10px',
    }
};

const ReviewsComments = (props) => {
    const {
        classes: {
            textArea,
            commentsContainer,
            saveComment,
            rateContainer,
        },
        comments,
        onAddCommentClick,
    } = props;

    return (
        <div>
            <h3>Reviews and Comments ({comments.length})</h3>
            <p>What do you think of this recipe?</p>
            <div className={commentsContainer}>
                <div className={rateContainer}>
                    <RecipeRate iconSize={35} isInteracted/>
                </div>
                <form>
                    <textarea className={textArea} placeholder="Leave your comment or review here"/>
                    <Button variant="contained"
                            color="primary"
                            onClick={onAddCommentClick()}
                            className={saveComment}>
                        Add Comment
                    </Button>
                </form>
            </div>
            {
                comments.map(comment => <Comment key={comment.date} comment={comment}/>)
            }
        </div>
    );
};

ReviewsComments.propTypes = {
    classes: PropTypes.object.isRequired,
    comments: PropTypes.array.isRequired,
    onAddCommentClick: PropTypes.func.isRequired,
};

export default withStyles(styles)(ReviewsComments);
