import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import List from '@material-ui/core/List';
import facebook from '../assets/images/facebook.svg';
import twitter from '../assets/images/twitter.svg';
import pinterest from '../assets/images/pinterest.svg';
import instagram from '../assets/images/instagram.svg';
import { getImageUrl } from '../utils/getImageUrl';

const styles = theme => ({
	root: {
		width: '100%',
	},
	heading: {
		fontSize: 16,
		fontWeight: '500',
		textTransform: 'uppercase',
		letterSpacing: 0.4
	},
	list: {
		width: '100%',
	},
	listItem: {
		paddingTop: 6,
		paddingBottom: 6
	},
	summary: {
		'&:hoover': {
		
		}
	},
	details: {
		padding: 0,
		'& a': {
			textDecoration: 'none'
		}
	},
	itemText: {
		fontSize: 14
	},
	flag: {
        height: 30
	}
});

class ControlledExpansionPanels extends Component {
	
	state = {
		expanded: null,
	};
	
	handleChange = panel => (event, expanded) => {
		this.setState({
			expanded: expanded ? panel : false,
		});
	};
	
	render() {
		const {
			classes: {
				root,
				heading,
				list,
				summary,
				details,
				listItem,
				flag
			},
			categories,
			areas,
			toggleDrawer
		} = this.props;
		const { expanded } = this.state;
		
		return (
			<div className={root}>
				<ExpansionPanel expanded={expanded === 'panel1'} onChange={this.handleChange('panel1')}>
					<ExpansionPanelSummary className={summary} expandIcon={<ExpandMoreIcon />}>
						<Typography className={heading}>World Cuisine</Typography>
					</ExpansionPanelSummary>
					<ExpansionPanelDetails className={details}>
						<List className={list}>
							{
								areas.map((area, index) => {
									const { strArea } = area;
									
									if(strArea === 'Unknown') {
										return;
									}
									return (
										<Link to={`/recipes/areas/${strArea}`} key={index}>
											<ListItem onClick={toggleDrawer} className={listItem} button>
												<ListItemText primary={strArea}/>
												<img className={flag} src={getImageUrl(strArea)} alt={strArea}/>
											</ListItem>
										</Link>
									)
								})
							}
						</List>
					</ExpansionPanelDetails>
				</ExpansionPanel>
				<ExpansionPanel expanded={expanded === 'panel2'} onChange={this.handleChange('panel2')}>
					<ExpansionPanelSummary className={summary} expandIcon={<ExpandMoreIcon />}>
						<Typography className={heading}>Dish Type</Typography>
					</ExpansionPanelSummary>
					<ExpansionPanelDetails className={details}>
						<List className={list}>
							{
								categories.map(category => {
									const { strCategory, idCategory } = category;
									
									return (
										<Link to={`/recipes/category/${strCategory}`} key={idCategory}>
											<ListItem onClick={toggleDrawer} className={listItem} button>
												<ListItemText primary={strCategory}/>
											</ListItem>
										</Link>
									)
								})
							}
						</List>
					</ExpansionPanelDetails>
				</ExpansionPanel>
				<p>Stay connected</p>
				<div>
					<a href="https://www.facebook.com" target="_blank" rel="noopener noreferrer"><img src={facebook} alt="facebook"/></a>
					<a href="https://www.twitter.com" target="_blank" rel="noopener noreferrer"><img src={twitter} alt="twitter"/></a>
					<a href="https://www.instagram.com" target="_blank" rel="noopener noreferrer"><img src={instagram} alt="instagram"/></a>
					<a href="https://www.pinterest.com" target="_blank" rel="noopener noreferrer"><img src={pinterest} alt="pinterest"/></a>
				</div>
			</div>
		)
	}
}

ControlledExpansionPanels.propTypes = {
	classes: PropTypes.object.isRequired,
	categories: PropTypes.array.isRequired,
	areas: PropTypes.array.isRequired,
	toggleDrawer: PropTypes.func.isRequired
};

export default withStyles(styles)(ControlledExpansionPanels);