import React from 'react';
import { RecipeCard } from '../recipe-card';
import { PropTypes } from 'prop-types';
import styled from 'styled-components';

export const RecipesContainer = styled.div`
	display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    justify-items: center;
	padding-top: 12px;
	border-top: 1px solid #e6e6e6;
`;

export const RecipesList = (props) => {
    const { recipes, toggleFavorite, rateRecipe } = props;

    return(
        <RecipesContainer>
            {
                recipes.map(recipe => {
                    const { idMeal } = recipe;

                    return (
                        <RecipeCard key={idMeal}
                                    recipe={recipe}
                                    toggleFavorite={toggleFavorite}
									rateRecipe={rateRecipe}/>
                    )
                })
            }
        </RecipesContainer>
    )
};


RecipesList.propTypes = {
    toggleFavorite: PropTypes.func.isRequired,
    rateRecipe: PropTypes.func.isRequired,
    recipes: PropTypes.array.isRequired,
};