import React, { Component } from 'react';
import queryString from 'query-string';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';
import { PropTypes } from 'prop-types';
import {
	dataFetchRequested,
	toggleFavorite,
	clearRecipes,
	rateRecipe,
} from '../../actions';
import { RecipesList } from './component';
import { updateStorage } from '../../utils/withStorage';

class RecipesListContainer extends Component {

    toggleFavorite = (recipeId) => () => {
        this.props.toggleFavorite(recipeId);
        updateStorage({recipeId, action: { name: 'toggleFavorite'}});
    };
    
    rateRecipe = (recipeDetails) => {
    	this.props.rateRecipe(recipeDetails);
	};
    
    isHomePage = () => ['/', '/home'].indexOf(this.props.location.pathname) > -1;

    componentDidMount() {
        if(this.isHomePage()) {
            return;
        }
        const {
            dataFetchRequested,
            match: {
                params: {
                    filterParam
                }
            },
            location: {
                pathname,
                search
            }
        } = this.props;
        const values = queryString.parse(search);
        let requestParams = {
            fetchType: 'filtered',
            params: {
                filterParam
            }
        };

        switch (true) {
            case pathname.indexOf('areas') > -1 :
                requestParams.params.type = 'area';
                break;
            case pathname.indexOf('category') > -1 :
                requestParams.params.type = 'category';
                break;
            default:
                requestParams.params.type = 'search';
                requestParams.params.filterParam = values.q;
        }

        dataFetchRequested(requestParams);
    }

    componentWillUnmount() {
        const { clearRecipes } = this.props;

        clearRecipes();
    }

    render() {
		const { recipes, dataFetchRequested } = this.props;
    	
        return (
            <RecipesList dataFetchRequested={dataFetchRequested}
                         toggleFavorite={this.toggleFavorite}
                         recipes={recipes}
						 rateRecipe={this.rateRecipe}/>
        );
    }
}

const mapStateToProps = (state) => {

    return {
        recipes: state.recipes,
    }
};

RecipesListContainer.propTypes = {
    dataFetchRequested: PropTypes.func.isRequired,
    toggleFavorite: PropTypes.func.isRequired,
	recipes: PropTypes.array.isRequired,
    location: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
    clearRecipes: PropTypes.func.isRequired,
    rateRecipe: PropTypes.func.isRequired,
};

export default compose(
	withRouter,
	connect(mapStateToProps, { dataFetchRequested, toggleFavorite, clearRecipes, rateRecipe })
)(RecipesListContainer);

