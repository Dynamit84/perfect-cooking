import React from 'react';
import PropTypes from "prop-types";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import IconButton from '@material-ui/core/IconButton';
import NextIcon from '@material-ui/icons/NavigateNext';

const ITEM_WIDTH = 131.25;

const styles = {
    avatar: {
        border: '1px solid #f7f6f4',
        width: 85,
        height: 85,
        'box-shadow': '3px 2px 5px 0 #c2c2c2'
    },
    itemText: {
        marginTop: 8,
        padding: 0,
        textAlign: 'center'
    },
    itemsList: {
        display: 'flex',
        overflow: 'hidden',
    },
    item: {
        display: 'flex',
        flexDirection: 'column',
        width: ITEM_WIDTH,
        paddingLeft: 22,
        paddingRight: 22,
		transition: 'margin-left 1s',
    },
    primary: {
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap'
    },
    itemLink: {
        width: '100%',
    },
    button: {
        width: 35,
        height: 35,
        position: 'absolute',
        right: 10,
        top: 'calc(50% - 20px)',
        zIndex: 100,
        backgroundColor: 'rgba(0,0,0,0.4)',
        '&.reversed': {
            left: 10,
            transform: 'rotate(180deg)',
        }
    },
    icon: {
        width: 22,
        height: 22,
        fill: '#fff',
    },
    container: {
        position: 'relative',
		borderBottom: '1px solid #e6e6e6',
    },
    wrapper: {
        padding: '0 25px',
        backgroundColor: '#d8d8d84a',
    }
};

const ArrowButton = (props) => {
    const { icon, button, reversed, onRightArrowClick, onLeftArrowClick } = props;

    return (
        <IconButton className={`${button} ${reversed && 'reversed'}`}
                    aria-label="Delete"
                    onClick={reversed ? onLeftArrowClick : onRightArrowClick}>
            <NextIcon className={icon} />
        </IconButton>
    );
};

const Carousel = (props) => {
    const {
        categories,
        decreaseOffset,
        increaseOffset,
        offset,
        classes: {
            avatar,
            itemText,
            itemsList,
            item,
            primary,
            itemLink,
            container,
            icon,
            button,
            wrapper,
        }
    } = props;

    return (
        <div className={container}>
            <ArrowButton button={button} icon={icon} onRightArrowClick={decreaseOffset}/>
            <div className={wrapper}>
                <List className={itemsList}>
                    {
                        categories.map(category => {
                            const {
                                idCategory,
                                strCategory,
                                strCategoryThumb
                            } = category;
                            const marginLeft = offset * ITEM_WIDTH;
                            return (

                                <ListItem className={item}
                                          key={idCategory}
                                          style={ {marginLeft: idCategory==='1' && `-${marginLeft}px`} } >
                                    <Link className={itemLink} to={`/recipes/category/${strCategory}`} >
                                        <Avatar className={avatar} src={strCategoryThumb}/>
                                    </Link>
                                    <Link className={itemLink} to={`/recipes/category/${strCategory}`} >
                                        <ListItemText classes={{root: itemText, primary}} primary={strCategory} />
                                    </Link>
                                </ListItem>
                            )
                        })
                    }
                </List>
            </div>
            <ArrowButton button={button} icon={icon} reversed onLeftArrowClick={increaseOffset}/>
        </div>
    )
};

Carousel.propTypes = {
    categories: PropTypes.array.isRequired,
    classes: PropTypes.object.isRequired,
    decreaseOffset: PropTypes.func.isRequired,
    increaseOffset: PropTypes.func.isRequired,
    offset: PropTypes.number.isRequired,
};

export default withStyles(styles)(Carousel);