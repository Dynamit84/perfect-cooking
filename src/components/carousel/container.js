import React, { Component } from 'react';
import Carousel from './component';
import PropTypes from 'prop-types';

export default class CarouselContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            offset: 0
        };
    }

    increaseOffset = () => {
    	if(this.state.offset > 3) {
    		return;
		}
        this.setState({
            offset: this.state.offset + 1
        });
    };

    decreaseOffset = () => {
		if(this.state.offset < 1) {
			return;
		}
        this.setState({
            offset: this.state.offset - 1
        });
    };

    render() {
        const { categories } = this.props;

        return <Carousel
            categories={categories}
            increaseOffset={this.increaseOffset}
            decreaseOffset={this.decreaseOffset}
            offset={this.state.offset}
        />;
    }
}

CarouselContainer.propTypes = {
    categories: PropTypes.array.isRequired,
};