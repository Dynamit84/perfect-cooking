import React, { Fragment } from 'react';
import Star from '@material-ui/icons/Star';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Tooltip from '@material-ui/core/Tooltip';

const styles = {
    star: {
        fill: '#999',
        '&.hovered': {
            fill: 'orange'
        },
        '&.rated': {
            fill: '#ff7e1a'
        },
        '&.interacted': {
            cursor: 'pointer',
        }
    },
    tooltip: {
        fontSize: 12,
    },
    container: {
        display: 'inline-block',
    }
};

const RecipeRate = (props) => {
    const {
        classes: { star, tooltip, container },
        hoverIndex,
        rate,
        onStarHover,
        onStarClick,
        iconSize,
        isInteracted,
    } = props;

    return (
        <Fragment>
            {
                isInteracted && <Tooltip id="tooltip-left" title="Rate recipe" placement="right" classes={{tooltip}}>
                    <div className={container} onMouseLeave={onStarHover()}>
                        {
                            Array.from(Array(5).keys()).map(num => {

                                return (
                                        <Star
                                            className={`${star} ${isInteracted && 'interacted'} ${  num < hoverIndex ? 'hovered' : '' } ${ num < rate ? 'rated' : '' }`}
                                            key={num}
                                            onMouseEnter={onStarHover(num + 1)}
                                            onClick={onStarClick(num + 1)}
                                            style={{fontSize: `${iconSize}px`}}
                                        />
                                )
                            })
                        }
                    </div>
                </Tooltip>
            }
            {
                !isInteracted && <div className={container}>
                    {
                        Array.from(Array(5).keys()).map(num => {

                            return (
                                <Star
                                    className={`${star} ${ num < rate ? 'rated' : '' }`}
                                    key={num}
                                    style={{fontSize: `${iconSize}px`}}
                                />
                            )
                        })
                    }
                </div>
            }
        </Fragment>

    )
};

RecipeRate.propTypes = {
    classes: PropTypes.object.isRequired,
    rate: PropTypes.number,
    hoverIndex: PropTypes.number,
    onStarClick: PropTypes.func,
    onStarHover: PropTypes.func,
    iconSize: PropTypes.number,
    isInteracted: PropTypes.bool,
};

export default withStyles(styles)(RecipeRate);