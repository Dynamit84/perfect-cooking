import React, { Component } from 'react';
import RecipeRate from './component';
import { PropTypes } from 'prop-types';
import { updateStorage } from '../../utils/withStorage';

export default class RecipeRateContainer extends Component {

    constructor() {
        super();
        this.state = {
            hoverIndex: 0,
            rateFromComment: 0
        };
    }

    onStarHover = (starNum = 0) => () => {
        this.setState({
            hoverIndex: starNum
        })
    };

    onStarClick = (starNum) => () => {
        this.setState({
            rateFromComment: starNum
        })
    };

    render() {
        const { hoverIndex, rateFromComment } = this.state;
        const { rate, iconSize, isInteracted } = this.props;

        return (
            <RecipeRate onStarHover={this.onStarHover}
                        onStarClick={this.onStarClick}
                        hoverIndex={hoverIndex}
                        rate={rate || rateFromComment}
                        iconSize={iconSize}
                        isInteracted={isInteracted}
            />
        );
    }
}

RecipeRateContainer.propTypes = {
    rate: PropTypes.number.isRequired,
    idMeal: PropTypes.string,
    rateRecipe: PropTypes.func,
    iconSize: PropTypes.number,
    isInteracted: PropTypes.boolean
};
