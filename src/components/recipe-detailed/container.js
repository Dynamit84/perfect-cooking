import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
	updateViewCounts,
	toggleFavoriteDetails,
	dataFetchRequested,
	rateRecipeDetailed,
} from "../../actions";
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';

import RecipeDetailed from './component';
import { updateStorage } from '../../utils/withStorage';

class RecipeDetailedContainer extends Component {

    componentDidMount() {
        const { match: { params : { recipeId } }, dataFetchRequested } = this.props;

        dataFetchRequested({
            fetchType: 'details',
            params: {
                type: 'details',
                filterParam: recipeId
            }
        });
        // updateStorage({recipeId, name: 'updateComments'});
    }

    toggleFavorite = (recipeId) => () => {
        this.props.toggleFavoriteDetails(recipeId);
        updateStorage({recipeId, action: { name: 'toggleFavorite'}});
    };
    
    rateRecipe = (recipeDetails) => {
    	const { rateRecipeDetailed } = this.props;
    	rateRecipeDetailed(recipeDetails);
	};

    render() {
        return (
            <RecipeDetailed toggleFavorite={this.toggleFavorite} recipe={this.props.recipe} rateRecipe={this.rateRecipe} />
        )
    }

}

const mapStateToProps = (state) => {

    return {
        recipe: state.recipeDetails
    }
};

RecipeDetailedContainer.propTypes = {
    updateViewCounts: PropTypes.func.isRequired,
    toggleFavoriteDetails: PropTypes.func.isRequired,
    dataFetchRequested: PropTypes.func.isRequired,
    recipe: PropTypes.object.isRequired,
	rateRecipeDetailed: PropTypes.func.isRequired,
};

export default compose(
	withRouter,
	connect(mapStateToProps, { updateViewCounts, toggleFavoriteDetails, dataFetchRequested, rateRecipeDetailed })
)(RecipeDetailedContainer);