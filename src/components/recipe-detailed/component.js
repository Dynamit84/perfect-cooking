import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { withLoader } from '../../utils/withLoader';
import Typography from '@material-ui/core/Typography';
import { RecipeRate } from '../recipe-rate';
import { withStyles } from '@material-ui/core/styles';
import { Ingredients } from '../ingredients'
import FavoriteRecipe from '../FavoriteRecipe';
import { getImageUrl } from '../../utils/getImageUrl';
import { Link } from 'react-router-dom';
import { ReviewsComments } from '../reviews-comments';

const styles = {
	pageContainer: {
		padding: 25,
	},
	summary: {
		display: 'flex',
		paddingBottom: 25,
		borderBottom: '1px solid #e6e6e6',
		justifyContent: 'space-between',
	},
	recipeSummary: {
		textAlign: 'center',
		marginRight: 20,
		position: 'relative',
	},
	recipeHeader: {
		marginTop: 25,
	},
	recipeDescription: {
		textAlign: 'left',
		marginTop: 10,
	},
	instructionsContainer: {
		marginTop: 15,
	},
	rateContainer: {
        display: 'inline-flex',
		alignItems: 'center',
	},
	ratesAmount: {
        fontWeight: 'bold',
		color: '#999',
		marginLeft: 5,
	}
};

class RecipeDetailed extends Component {
	
	static replaceToPlayVideo(url) {
		return url.replace('watch?v=', 'embed/');
	}
	
	formIngredientsArray() {
		const { recipe } = this.props;
		return Object.entries(recipe).reduce((result, entry) => {
			const [key, value] = entry;
			if(key.includes('strIngredient')) {
				const ingrIndex = key.match(/\d+/g)[0];
				value && result.push([[value], recipe[`strMeasure${ingrIndex}`]])
			}
			return result;
		}, []);
	}
	
	render() {
		const {
            recipe,
			recipe: {
				strMeal,
				strYoutube,
				description,
				idMeal,
				isFavourite,
				strInstructions,
				rate,
                strArea,
				comments,
			},
			classes: {
                pageContainer,
				summary,
                recipeSummary,
                recipeHeader,
                recipeDescription,
                instructionsContainer,
                rateContainer,
                ratesAmount,
			},
			toggleFavorite,
			rateRecipe,
		} = this.props;
		const videoUrl = RecipeDetailed.replaceToPlayVideo(strYoutube);
		const ingrArray = this.formIngredientsArray();

		return (
			<div className={pageContainer}>
				<div className={summary}>
					<div className={recipeSummary}>
						<Typography gutterBottom variant="headline" className={recipeHeader} component="h1">
							{strMeal}
						</Typography>
						<div className={rateContainer}>
                            <RecipeRate rate={rate} iconSize={28}/>
                            <span className={ratesAmount}>({comments.length})</span>
						</div>
						<Typography gutterBottom className={recipeDescription} component="p">
							{description}
						</Typography>
						<FavoriteRecipe onFavoriteClick={toggleFavorite}
										isFavourite={isFavourite}
										idMeal={idMeal}/>
						<Link to={`/recipes/areas/${strArea}`}>
                        	<img src={getImageUrl(strArea)} alt={strArea}/>
						</Link>
					</div>
					<div>
						<iframe id="ytplayer" type="text/html" width="470" height="260"
								src={videoUrl}
								title={strMeal}
								frameBorder="0">
						</iframe>
					</div>
				</div>
				<Ingredients ingredients={ingrArray}/>
                <div className={instructionsContainer}>
                    <Typography gutterBottom variant="headline" component="h2">
                        Instructions
                    </Typography>
                    <Typography gutterBottom className='recipe-description' component="p">
                        {strInstructions}
                    </Typography>
				</div>
				<ReviewsComments comments={comments} recipe={recipe}/>
			</div>
		)
	}
	
}

RecipeDetailed.propTypes = {
	classes: PropTypes.object.isRequired,
	recipe: PropTypes.object.isRequired,
	toggleFavorite: PropTypes.func.isRequired,
	rateRecipe: PropTypes.func.isRequired,
};

export default compose(withLoader('recipe'), withStyles(styles))(RecipeDetailed);