import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import Visibility from '@material-ui/icons/Visibility';
import { RecipeRate } from '../recipe-rate';
import FavoriteRecipe from '../FavoriteRecipe';
import { getImageUrl } from '../../utils/getImageUrl';
import Comment from '@material-ui/icons/Comment';

const StyledLink = styled.a`
	text-decoration: none;
	color: #028198;
	:hover {
		text-decoration: underline;
	}
`;

const RecipeDescription = styled.div`
	font-size: 14px;
	margin: 5px 0 8px;
`;

const LinkContainer = styled.div`
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
	font-size: 12px;
	color: #2d2d2d;
`;

const RateCountryContainer = styled.div`
	display: flex;
    align-items: center;
    justify-content: space-between;;
`;

const Count = styled.span`
	vertical-align: middle;
`;

const FlagContainer = styled(Link)`
	height: 30px;
`;

const Flag = styled.img`
	height: 30px;
`;

const styles = {
    card: {
        width: 280,
        margin: '15px 10px',
        position: 'relative',
    },
    media: {
        height: 0,
        paddingTop: '65%',
    },
    recipeName: {
        fontSize: 18,
        color: '#4d4d4d',
        fontWeight: 'bold',
        display: 'inline',
    },
    cardContainer: {
        paddingLeft: 15,
        paddingRight: 15,
        color: '#7f7f7f',
        '&:last-child': {
            paddingBottom: 12
        }
    },
    icon: {
        fill: '#8E9295',
        marginRight: 3,
        fontSize: 16,
        verticalAlign: 'middle'
    },
    link: {
        textDecoration: 'none',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        display: 'block',
    },
    iconsWrapper: {
        fontSize: 12,
        marginTop: 5,
        display: 'flex',
        alignItems: 'center',
    },
    iconContainer: {
        display: 'inline-block',

        '&:first-child': {
            marginRight: 5,
        }
    },
    comment: {
        fontSize: 15,
    }
};

const RecipeCard = (props) => {
    const {
        classes: {
            link,
            card,
            icon,
            cardContainer,
            recipeName,
            media,
            iconsWrapper,
            iconContainer,
            comment,
        },
        toggleFavorite,
		rateRecipe,
        recipe: {
            strMealThumb,
            strMeal,
            idMeal,
            strSource,
            viewCount,
            isFavourite,
            description,
            strArea,
            rate,
			comments,
        }
    } = props;

    return (
        <Card className={card}>
            <Link to={`/recipe/${idMeal}`}>
                <CardMedia
                    className={media}
                    image={strMealThumb}
                    title={strMeal}/>
            </Link>
            <FavoriteRecipe onFavoriteClick={toggleFavorite}
                            isFavourite={isFavourite}
                            idMeal={idMeal}
                            isCard/>
            <CardContent className={cardContainer}>
                <Link className={link} to={`/recipe/${idMeal}`}>
                    <Typography className={recipeName} gutterBottom variant="headline" component="h2">
                        {strMeal}
                    </Typography>
                </Link>
                <RateCountryContainer>
                    <RecipeRate rate={rate} idMeal={idMeal} rateRecipe={rateRecipe}/>
                    {
                        strArea && (
                            <FlagContainer to={`/recipes/areas/${strArea}`}>
                                <Flag src={getImageUrl(strArea)} alt={strArea}/>
                            </FlagContainer>
                        )
                    }
                </RateCountryContainer>
                <RecipeDescription>{`${description.slice(0,75)}...`}</RecipeDescription>
                <LinkContainer>
                    <span>Sourced from:</span>
                    <StyledLink href={strSource}> {strSource}</StyledLink>
                </LinkContainer>
                <div className={iconsWrapper}>
                    <div className={iconContainer}>
                        <Visibility className={icon} />
                        <Count>{viewCount}</Count>
                    </div>
                    <div className={iconContainer}>
                        <Comment className={`${icon} ${comment}`} />
                        <span>{comments.length}</span>
                    </div>
                </div>
            </CardContent>
        </Card>
    );
};

RecipeCard.propTypes = {
    classes: PropTypes.object.isRequired,
    recipe: PropTypes.object.isRequired,
    toggleFavorite: PropTypes.func.isRequired,
    rateRecipe: PropTypes.func.isRequired,
};

export default withStyles(styles)(RecipeCard);