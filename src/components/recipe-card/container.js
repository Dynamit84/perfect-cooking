import React, { Component } from 'react';
import RecipeCard from './component';
import PropTypes from 'prop-types';

export default class RecipeCardContainer extends Component {

    render() {
        const { recipe, toggleFavorite, rateRecipe } = this.props;

        return (
            <RecipeCard recipe={recipe} toggleFavorite={toggleFavorite} rateRecipe={rateRecipe}/>
        );
    }
}

RecipeCardContainer.propTypes = {
    recipe: PropTypes.object.isRequired,
    toggleFavorite: PropTypes.func.isRequired,
    rateRecipe: PropTypes.func.isRequired,
};