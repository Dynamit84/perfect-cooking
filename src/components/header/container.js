import React, { Component } from 'react';
import Header from './component';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { dataFetchRequested, toggleDrawer } from '../../actions';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';

class HeaderContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            searchValue: ''
        }
    }

    toggleDrawer = () => {
        this.props.toggleDrawer();
    };

    searchRecipe = () => {
    	this.props.history.push(`/search/results?q=${this.state.searchValue}`);
        /*this.props.dataFetchRequested({
			fetchType: 'search',
			params: {
				filterParam: this.state.searchValue
			}
		});*/
    };

    changeSearchValue = (event) => {
        this.setState({
            searchValue: event.target.value
        })
    };

    render() {

        return (
            <Header toggleDrawer={this.toggleDrawer}
                    searchRecipe={this.searchRecipe}
                    inputValue={this.state.searchValue}
                    onInputChange={this.changeSearchValue}
            />
        );
    }
}

HeaderContainer.propTypes = {
    toggleDrawer: PropTypes.func.isRequired,
    dataFetchRequested: PropTypes.func.isRequired,
	history: PropTypes.object.isRequired,
};

export default compose(withRouter, connect(null, { dataFetchRequested, toggleDrawer }))(HeaderContainer);