import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import { Link } from 'react-router-dom';
import logo from '../../assets/images/logo.svg';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Button from '@material-ui/core/Button';
import SearchIcon from '@material-ui/icons/Search';

const styles = {
    header: {
        height: 65,
        padding: '0 20px',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    logoLink: {
        display: 'inline-block'
    },
    button: {
        marginRight: 10
    },
    leftContainer: {
        display: 'flex',
        alignItems: 'center',
    },
    searchIcon: {

    },
    searchBtn: {

    },
    searchInput: {
        borderRadius: 4,
        border: 'none',
        width: 300,
        marginRight: 11,
        boxShadow: '0px 1px 5px 0px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12)',
        fontSize: 16,
        boxSizing: 'border-box',
        padding: '1px 7px',
    },
    rightContainer: {
        display: 'flex',
        height: 35,
    }
};

const Header = (props) => {
    const {
        classes: {
            logoLink,
            button,
            header,
            leftContainer,
            searchIcon,
            searchBtn,
            searchInput,
            rightContainer,
        },
        toggleDrawer,
        inputValue,
        onInputChange,
		searchRecipe,
    } = props;

    return (
        <AppBar className={header} color="default">
            <div className={leftContainer}>
                <IconButton color="inherit"
                            aria-label="Menu"
                            onClick={toggleDrawer}
                            className={button}
                >
                    <MenuIcon />
                </IconButton>
                <Link className={logoLink} to={'/'}>
                    <img src={logo} alt="Logo"/>
                </Link>
            </div>
            <div className={rightContainer}>
                <input placeholder="Find a recipe"
                       className={searchInput}
                       value={inputValue}
                       onChange={onInputChange}
                />
                <Button variant="contained"
						size="small"
						className={searchBtn}
						onClick={searchRecipe}
				>
                    <SearchIcon className={searchIcon}/>
                    Search
                </Button>
            </div>
        </AppBar>
    )
};

Header.propTypes = {
    classes: PropTypes.object.isRequired,
    toggleDrawer: PropTypes.func.isRequired,
    searchRecipe: PropTypes.func.isRequired,
    inputValue: PropTypes.string.isRequired,
    onInputChange: PropTypes.func.isRequired,
};

export default withStyles(styles)(Header);