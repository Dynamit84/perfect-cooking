import React from 'react';
import PropTypes from "prop-types";
import styled from 'styled-components';
import { withLoader } from '../../utils/withLoader';
import background from '../../assets/images/home-header.jpg';
import { Carousel } from '../carousel';
import { RecipesList } from '../recipes-list/index';

export const StylingWrapper = styled.div`
    background: linear-gradient(to right, rgba(0,0,0,0.85) 0%, rgba(0,0,0,0.85) 14%, rgba(0,0,0,0.16) 45%, transparent 100%);
    padding: 45px 0 35px 40px;
    height: 100%;
    box-sizing: border-box;
`;
export const HomePageDescription = styled.div`
	color: #fff;
	height: 200px;
	background: url(${background}) no-repeat center right;
	background-size: cover;
`;
export const SiteHeader = styled.h1`
	font-size: 35px;
	font-weight: 300;
	margin: 0;
`;
export const SiteDescription = styled.p`
	font-size: 16px;
	font-weight: 300;
	max-width: 50%;
`;
export const LatelyAdded = styled.h3`
	font-size: 17px;
	margin: 20px 0 10px 20px;
`;

const siteHeader = "Explore recipes";
const siteDescription = "Whether you’re looking for healthy recipes or ideas to use up last night’s chicken, we’ve more than 7000 tested recipes to choose from, so you’re sure to find the perfect dish.";
const latelyAdded = "Lately added recipes";

const HomePage = (props) => {
	const { categories } = props;

	return (
		<div>
			<HomePageDescription>
				<StylingWrapper>
					<SiteHeader>{siteHeader}</SiteHeader>
					<SiteDescription>{siteDescription}</SiteDescription>
				</StylingWrapper>
			</HomePageDescription>
			<Carousel categories={categories}/>
			<LatelyAdded>{latelyAdded}</LatelyAdded>
			<RecipesList/>
		</div>
	)
};

const propTypes = {
	categories: PropTypes.array.isRequired,
	recipes: PropTypes.array.isRequired,
	toggleFavorite: PropTypes.func.isRequired,
};

HomePage.propTypes = propTypes;

export default withLoader('recipes')(HomePage);