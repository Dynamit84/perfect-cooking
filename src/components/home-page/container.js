import React, { Component } from 'react';
import Home from './component';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from "prop-types";
import { toggleFavorite, dataFetchRequested } from "../../actions";

class HomeContainer extends Component {
	
	componentDidMount() {
        const { dataFetchRequested } = this.props;

        dataFetchRequested({ fetchType: 'latest'});
	}

    toggleFavorite = (recipeId) => () => {
        this.props.toggleFavorite(recipeId);
    };
	
	render() {
        const { recipes, categories } = this.props;

		return <Home recipes={recipes}
					 categories={categories}
                     toggleFavorite={this.toggleFavorite}
		/>;
	}
}

const mapStateToProps = (state) => {

    return {
        recipes: state.recipes,
        categories: state.categories,
    }
};

HomeContainer.propTypes = {
    categories: PropTypes.array.isRequired,
    recipes: PropTypes.array.isRequired,
    toggleFavorite: PropTypes.func.isRequired,
};

export default compose(withRouter, connect(mapStateToProps, { toggleFavorite, dataFetchRequested }))(HomeContainer);