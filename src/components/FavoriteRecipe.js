import React from 'react';
import Favorite from '@material-ui/icons/Favorite';
import styled from 'styled-components';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Tooltip from '@material-ui/core/Tooltip';

const FavoriteContainer = styled.div`
	position: absolute;
	background-color: rgba(0,0,0,0.7);
    width: 55px;
    height: 55px;
    border-radius: 50%;
    cursor: pointer;
    top: 15px;
    
    :hover {
    	background-color: rgba(0,0,0,0.8);
    }
    
    &.card {
    	right: 15px;
    }
`;

const styles = {
	favorite: {
		position: 'absolute',
		top: '50%',
		left: '50%',
		transform: 'translate(-48%, -45%)',
		fill: '#fff',
		width: 30,
		height: 30,
		transition: 'width, height .3s',
		'&:hover': {
			width: 40,
			height: 40,
		},
		'&.liked': {
			fill: '#ff7e1a'
		}
	},
	tooltip: {
		fontSize: 13,
		padding: '6px 8px',
	},
};

const FavoriteRecipe = (props) => {
	const {
		classes: {
			favorite,
			tooltip,
		},
		onFavoriteClick,
		idMeal,
		isFavourite,
		isCard
	} = props;
	const tooltipText = isFavourite ? 'Remove from favorites' : 'Add to favorites';
	
	return (
        <Tooltip id="tooltip-top" title={tooltipText} placement="top-start" classes={{tooltip}}>
			<FavoriteContainer className={`${isCard && 'card'}`}>
				<Favorite onClick={onFavoriteClick(idMeal)}
						  className={`${favorite} ${isFavourite && 'liked'}`}/>
			</FavoriteContainer>
		</Tooltip>
	)
};

FavoriteRecipe.propTypes = {
	classes: PropTypes.object.isRequired,
	isFavourite: PropTypes.bool.isRequired,
	onFavoriteClick: PropTypes.func.isRequired,
	idMeal: PropTypes.string.isRequired,
	isCard: PropTypes.bool
};

export default withStyles(styles)(FavoriteRecipe);