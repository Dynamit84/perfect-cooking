import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import ControlledExpansionPanels from '../ControlledExpansionPanels';

const styles = {
    list: {
        width: 250,
        height: '100%',
        position: 'relative',
        'padding-top': 40,
        boxSizing:'border-box'
    },
    button: {
        width: 30,
        height: 30,
        transition: 'transform .5s ease',
        position: 'absolute',
        right: 0,
        top: 0,
        '&:hover': {
            transform: 'rotate(90deg)'
        }
    },
    icon: {
        width: 16,
        height: 16
    },
    height: {
        height: '100%'
    }
};

const TemporaryDrawer = (props) => {
    const {
        classes: {
            list,
            button,
            icon,
            height
        },
        isOpen,
        toggleDrawer,
        categories,
        areas
    } = props;

    return (
        <Drawer open={isOpen} onClose={toggleDrawer}>
            <div tabIndex={0} className={height}>
                <div className={list} >
                    <IconButton onClick={toggleDrawer} className={button} aria-label="Delete">
                        <CloseIcon className={icon} />
                    </IconButton>
                    <ControlledExpansionPanels categories={categories}
                                               areas={areas}
                                               toggleDrawer={toggleDrawer}
                    />
                </div>
            </div>
        </Drawer>
    )
};

TemporaryDrawer.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    toggleDrawer: PropTypes.func.isRequired,
    categories: PropTypes.array.isRequired,
    areas: PropTypes.array.isRequired
};

export default withStyles(styles)(TemporaryDrawer);