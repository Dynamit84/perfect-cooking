import React, { Component } from 'react';
import TemporaryDrawer from './component';
import { toggleDrawer } from '../../actions';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';

class TemporaryDrawerContainer extends Component {
	
	toggleDrawer = () => {
		this.props.toggleDrawer();
	};
	
	render() {
		const { isDrawerOpen, categories, areas } = this.props;
		
		return (
			<TemporaryDrawer isOpen={isDrawerOpen}
							 toggleDrawer={this.toggleDrawer}
							 categories={categories}
							 areas={areas}
			/>
		);
	}
}

const mapStateToProps = (state) => {
	
	return {
		isDrawerOpen: state.isDrawerOpen,
		categories: state.categories,
		areas: state.areas
	}
};

TemporaryDrawerContainer.propTypes = {
	isDrawerOpen: PropTypes.bool.isRequired,
	toggleDrawer: PropTypes.func.isRequired,
	categories: PropTypes.array.isRequired,
	areas: PropTypes.array.isRequired
};

export default connect(mapStateToProps, { toggleDrawer })(TemporaryDrawerContainer);