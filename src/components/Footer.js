import React from 'react';
import { withStyles } from '@material-ui/core/styles';

const styles = {
	footer: {
		height: 50,
	}
};

const Footer = () => {
	
	return (
		<footer><p>© 2018 AllRecipes.com is part of the Allrecipes Food Group. All Rights Reserved.</p></footer>
	);
};

export default withStyles(styles)(Footer);