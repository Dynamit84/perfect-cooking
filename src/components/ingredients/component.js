import React from 'react';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { getImageUrl } from '../../utils/getImageUrl';

const styles = {
    avatar: {
        border: '1px solid #f7f6f4',
        width: 65,
        height: 65,
        'box-shadow': '3px 2px 5px 0 #c2c2c2'
    },
    itemText: {
        display: 'flex',
        'flex-direction': 'column-reverse'
    },
    itemsList: {
        display: 'flex',
        'flex-wrap': 'wrap'
    },
    item: {
        'min-width': 200,
        width: '25%'
    },
    container: {
        marginTop: 15,
        borderBottom: '1px solid #e6e6e6'
    }
};

const Ingredients = (props) => {

    const { classes: { avatar, itemText, item, itemsList, container }, ingredients } = props;

    return (
        <div className={container}>
            <Typography gutterBottom variant="headline" component="h2">
                Ingredients
            </Typography>
            <List className={itemsList}>
                {
                    ingredients.map((ingr, key) => {

                        return (
                            <ListItem className={item} key={key}>
                                <Avatar className={avatar} src={getImageUrl(ingr[0], 'ingredients')}/>
                                <ListItemText className={itemText} primary={ingr[0]}  secondary={ingr[1]} />
                            </ListItem>
                        )
                    })
                }
            </List>
        </div>
    )
};

Ingredients.propTypes = {
    classes: PropTypes.object.isRequired,
    ingredients: PropTypes.array.isRequired
};

export default withStyles(styles)(Ingredients);