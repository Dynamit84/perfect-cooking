import React, { Component } from 'react';
import Ingredients from './component';
import PropTypes from 'prop-types';

export default class IngredientsContainer extends Component {

    render() {

        return (
            <Ingredients ingredients={this.props.ingredients} />
        );
    }
}

IngredientsContainer.propTypes = {
    ingredients: PropTypes.array.isRequired
};
