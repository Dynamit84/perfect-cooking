import { call, put, takeEvery } from 'redux-saga/effects';
import {
    DATA_FETCH_REQUESTED
} from "../constants";
import { fetchData } from "../api";
import {
	categoriesFetchSucceeded,
	areasFetchSucceeded,
    filteredFetchSucceeded,
	detailsFetchSucceeded
} from '../actions';
import { syncWithStorage } from "../utils/withStorage";

const successHash = {
    categories: (data) => categoriesFetchSucceeded(data),
    latest: (data) => filteredFetchSucceeded(data),
    areas: (data) => areasFetchSucceeded(data),
    filtered: (data) => filteredFetchSucceeded(data),
	details: (data) => detailsFetchSucceeded(data),
};

function* getApiData(action) {
    const { payload: { fetchType, params } } = action;
    try {
        let data = yield call(fetchData, fetchType, params);
        if(['latest', 'filtered', 'details'].includes(fetchType)) {
			data = yield call(syncWithStorage, data);
		}
        yield put(successHash[fetchType](data));
    } catch (e) {
        console.log(e);
    }
}

export function* watchFetchData() {
    yield takeEvery(DATA_FETCH_REQUESTED, getApiData);
}
