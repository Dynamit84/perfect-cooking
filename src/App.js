import React, { Component, Fragment } from 'react';
import { Home } from './components/home-page';
import { RecipeDetailedContainer } from './components/recipe-detailed';
import { Route, Switch, withRouter } from 'react-router-dom';
import { dataFetchRequested } from './actions';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import { compose } from 'redux';
import {HeaderContainer as Header } from './components/header';
import { RecipesList } from './components/recipes-list';
import { TemporaryDrawer } from './components/temporary-drawer';
import { toggleDrawer } from './actions';
import Footer from './components/Footer';

class App extends Component {
	componentDidMount() {
		const { dataFetchRequested } = this.props;

		dataFetchRequested({ fetchType: 'categories'});
		dataFetchRequested({ fetchType: 'areas'});
	}
	
	render() {
		
		return (
			<Fragment>
				<Header/>
				<TemporaryDrawer/>
				<Switch>
					<Route exact path='/' component={Home} />
					<Route path={'/home'} component={Home} />
					<Route path={'/recipe/:recipeId'} component={RecipeDetailedContainer} />
					<Route path={'/search/results'} component={RecipesList} />
					<Route exact path={'/recipes/:filter/:filterParam'} component={RecipesList} />
				</Switch>
				<Footer/>
			</Fragment>
		);
	}
}

const mapStateToProps = (state) => {
	
	return {
		isDrawerOpen: state.isDrawerOpen,
		categories: state.categories,
		areas: state.areas
	}
};

App.propTypes = {
    dataFetchRequested: PropTypes.func.isRequired,
};

export default compose(withRouter, connect(mapStateToProps, { dataFetchRequested, toggleDrawer }))(App);
