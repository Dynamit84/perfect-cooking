import { combineReducers } from 'redux';
import { categories } from './categories';
import { recipes } from "./recipes";
import { areas } from "./areas";
import { isDrawerOpen } from './drawer';
import { recipeDetails } from './recipeDetails';

export default combineReducers ({
    categories,
	recipes,
	areas,
	isDrawerOpen,
	recipeDetails
    //router: routerReducer
});