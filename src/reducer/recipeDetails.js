import {
	DETAILS_FETCH_SUCCEEDED,
	TOGGLE_FAVORITE_DETAILS,
	RATE_RECIPE_DETAILED,
    SAVE_COMMENT
} from "../constants";
import { handleActions } from 'redux-actions';

export const recipeDetails = handleActions({
    [DETAILS_FETCH_SUCCEEDED]: (state, { payload }) => (
        {
            ...state,
            ...payload[0]
        }
    ),
    [TOGGLE_FAVORITE_DETAILS]: (state) => (
        {
            ...state,
            isFavourite: !state.isFavourite
        }
    ),
	[RATE_RECIPE_DETAILED]: (state, { payload: { rate } }) => (
        {
            ...state,
            rate: rate
        }
    ),
    [SAVE_COMMENT]: (state, { payload: { comment } }) => (
        {
            ...state,
            comments: [...state.comments, comment]
        }
    ),
}, {});