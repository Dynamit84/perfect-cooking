import { CATEGORIES_FETCH_SUCCEEDED } from "../constants";
import { handleActions } from 'redux-actions';

export const categories = handleActions({
	[CATEGORIES_FETCH_SUCCEEDED]: (state, { payload }) => ([
		...state,
		...payload.categories
	])
}, []);