import { AREAS_FETCH_SUCCEEDED } from "../constants";
import { handleActions } from 'redux-actions';

export const areas = handleActions({
	[AREAS_FETCH_SUCCEEDED]: (state, { payload }) => ([
		...state,
		...payload.meals
	])
}, []);