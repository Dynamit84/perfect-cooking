import {
	FILTERED_FETCH_SUCCEEDED,
	TOGGLE_FAVORITE,
	CLEAR_RECIPES,
	RATE_RECIPE
} from "../constants";
import { handleActions } from 'redux-actions';

export const recipes = handleActions({
	[FILTERED_FETCH_SUCCEEDED]: (state, { payload }) => (
		[
			...payload
		]
	),
	[TOGGLE_FAVORITE]: (state, { payload }) => (
		state.map(recipe => {
			if (recipe.idMeal === payload) {

				return {
					...recipe,
					isFavourite: !recipe.isFavourite
				}
			}

			return recipe;
		})
	),
	[CLEAR_RECIPES]: () => [],
	[RATE_RECIPE]: (state, { payload: { idMeal, rate } }) => (
		state.map(recipe => {
			if (recipe.idMeal === idMeal) {

				return {
					...recipe,
					rate
				}
			}

			return recipe;
		})
	)
}, []);