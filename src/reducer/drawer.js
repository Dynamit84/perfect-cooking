import { TOGGLE_DRAWER } from "../constants";
import { handleActions } from 'redux-actions';

export const isDrawerOpen = handleActions({
	[TOGGLE_DRAWER]: (state) => !state
}, false);