import { createActions } from 'redux-actions';
import * as constants from './constants';

export const {
	categoriesFetchSucceeded,
	areasFetchSucceeded,
	updateViewCounts,
	toggleFavorite,
    filteredFetchSucceeded,
	dataFetchRequested,
	toggleDrawer,
	detailsFetchSucceeded,
	clearRecipes,
    toggleFavoriteDetails,
	rateRecipe,
	rateRecipeDetailed,
	saveComment,
} = createActions(
	constants.CATEGORIES_FETCH_SUCCEEDED,
	constants.AREAS_FETCH_SUCCEEDED,
	constants.UPDATE_VIEW_COUNTS,
	constants.TOGGLE_FAVORITE,
	constants.FILTERED_FETCH_SUCCEEDED,
	constants.DATA_FETCH_REQUESTED,
	constants.TOGGLE_DRAWER,
	constants.DETAILS_FETCH_SUCCEEDED,
	constants.CLEAR_RECIPES,
	constants.TOGGLE_FAVORITE_DETAILS,
	constants.RATE_RECIPE,
	constants.RATE_RECIPE_DETAILED,
	constants.SAVE_COMMENT,
);