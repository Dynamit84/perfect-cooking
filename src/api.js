const urlHash = {
    categories: 'https://www.themealdb.com/api/json/v1/1/categories.php',
    latest: 'https://www.themealdb.com/api/json/v1/1/latest.php',
    areas: 'https://www.themealdb.com/api/json/v1/1/list.php?a=list',
	details: 'https://www.themealdb.com/api/json/v1/1/lookup.php?i=',
    area: 'https://www.themealdb.com/api/json/v1/1/filter.php?a=',
    category: 'https://www.themealdb.com/api/json/v1/1/filter.php?c=',
	search: 'https://www.themealdb.com/api/json/v1/1/search.php?s=',
};

export const fetchData = async (fetchType, params) => {
    try {
    	const url = params ? `${urlHash[params.type]}${params.filterParam}` : urlHash[fetchType];
        const response = await fetch(url);

        return await response.json();
    } catch(e) {
        console.log(e);
    }
};