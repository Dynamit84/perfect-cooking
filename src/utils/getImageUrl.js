const generalUrl = 'https://www.themealdb.com/images';

export const getImageUrl = (imageName, type) => {
	if(type === 'ingredients') {
		
		return `${generalUrl}/${type}/${imageName}.png`;
	}
	
	return `${generalUrl}/icons/flags/big/new/${imageName}.png`;
};