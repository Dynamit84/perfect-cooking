const NAMES = [
	{
		"name": "Lucas"
	},
	{
		"name": "Katina"
	},
	{
		"name": "Howe"
	},
	{
		"name": "Nellie"
	},
	{
		"name": "Charmaine"
	},
	{
		"name": "Thelma"
	},
	{
		"name": "Shaw"
	},
	{
		"name": "Sylvia"
	},
	{
		"name": "Faye"
	},
	{
		"name": "Dale"
	},
	{
		"name": "Marian"
	},
	{
		"name": "Torres"
	},
	{
		"name": "Silvia"
	},
	{
		"name": "Hazel"
	},
	{
		"name": "Hunter"
	},
	{
		"name": "Allen"
	},
	{
		"name": "Marguerite"
	},
	{
		"name": "Natalie"
	},
	{
		"name": "Schroeder"
	},
	{
		"name": "Richards"
	},
	{
		"name": "Macdonald"
	},
	{
		"name": "Ada"
	},
	{
		"name": "Raymond"
	},
	{
		"name": "Vaughan"
	},
	{
		"name": "Roberta"
	},
	{
		"name": "Taylor"
	},
	{
		"name": "Carey"
	},
	{
		"name": "Samantha"
	},
	{
		"name": "Ruiz"
	},
	{
		"name": "Rochelle"
	},
	{
		"name": "Russo"
	},
	{
		"name": "Pope"
	},
	{
		"name": "Lena"
	},
	{
		"name": "Noble"
	},
	{
		"name": "Moore"
	},
	{
		"name": "Connie"
	},
	{
		"name": "Schultz"
	},
	{
		"name": "Cohen"
	},
	{
		"name": "Jackson"
	},
	{
		"name": "Lorena"
	},
	{
		"name": "Marci"
	},
	{
		"name": "Farrell"
	},
	{
		"name": "Callahan"
	},
	{
		"name": "Flores"
	},
	{
		"name": "Bruce"
	},
	{
		"name": "Debora"
	},
	{
		"name": "April"
	},
	{
		"name": "Santana"
	},
	{
		"name": "Solomon"
	},
	{
		"name": "Ila"
	}
];

const COMMENTS_MOCKS = [
    {
        rate: 4,
        likes: 3,
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium aperiam aspernatur delectus dolorem doloribus id ipsam iste molestiae nemo nobis, odit pariatur provident tenetur, ullam ut vel. Earum, nisi?'
    },
    {
        rate: 5,
        likes: 7,
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium aperiam aspernatur delectus dolorem doloribus id ipsam iste molestiae nemo nobis, odit pariatur provident tenetur, ullam ut vel. '
    },
    {
        rate: 3,
        likes: 1,
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium aperiam aspernatur delectus dolorem doloribus id.'
    },
    {
        rate: 4,
        likes: 5,
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium aperiam aspernatur delectus dolorem doloribus id ipsam iste molestiae nemo nobis, odit pariatur.'
    },
];

const generateRandomNumber = (max) => {
	
	return Math.floor(Math.random() * Math.floor(max));
};

const generateRandomDate =	(start, end) => {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
};

export const generateComments = () => {
	let comments = [];

	for (let i=0; generateRandomNumber(20); i++) {
		comments = [
			...comments,
            {
                reviewer: NAMES[generateRandomNumber(NAMES.length)],
                date: generateRandomDate(new Date(2014, 0, 1), new Date()),
                ...COMMENTS_MOCKS[generateRandomNumber(COMMENTS_MOCKS.length)]
            }
		]
	}
	
	return comments;
};