import { generateComments } from './generateComments';

const getDataFromStorage = () => JSON.parse(localStorage['recipes'] || null) || [];
const getRecipeRate = (comments) => {
	if(!comments.length) {
		return 0;
	}

	return Math.floor(comments.reduce((accumulator, comment) => {

		return (accumulator + comment.rate);
	}, 0) / comments.length);
};

export const updateStorage = (data) => {
	const { recipeId, action: { name } = {} } = data;
	const storageData = getDataFromStorage();
	const newData = storageData.map(data => {
		if(recipeId === data.idMeal) {

			switch (name) {
				case 'toggleFavorite':
                    return {
                        ...data,
                        isFavourite: !data.isFavourite
                    };
				default:
                    return {
                        ...data,
                        viewCount: data.viewCount + 1
                    }
			}
		}
		
		return data;
	});
	localStorage['recipes'] = JSON.stringify(newData);
};

export const syncWithStorage = (data) => {
	const storageData = getDataFromStorage();
    const isDataInStorage = Boolean(storageData.length);
    let extendedRecipes = isDataInStorage ? [...storageData] :  [];
	
	const recipes = data.meals.map(recipe => {
		const recipeDataFromStorage = storageData.find(data => recipe.idMeal === data.idMeal);
		const isRecipeInStorage = Boolean(recipeDataFromStorage);
		
		recipe.description = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium, ad aliquam eos et ' +
			'ex illo incidunt laboriosam molestiae nam officiis, omnis perferendis praesentium quae quasi quisquam r' +
			'ecusandae reprehenderit similique? Ab aliquam doloremque excepturi fugiat laboriosam molestiae quo ' +
			'velit voluptatum. Quos.';
		
		if(!isRecipeInStorage) {
			const comments = generateComments();
			const rate = getRecipeRate(comments);
			
			const extendRecipe = {
                isFavourite: false,
            	viewCount: 0,
				comments,
				rate,
			};
            extendedRecipes = [...extendedRecipes, {idMeal: recipe.idMeal, ...extendRecipe}];
			return {
				...recipe,
				...extendRecipe
            };
		} else {
			const { isFavourite, viewCount, rate, comments } = recipeDataFromStorage;
			recipe.isFavourite = isFavourite;
			recipe.viewCount = viewCount;
			recipe.rate = rate;
			recipe.comments = comments;
		}
		
		return recipe;
	});

    localStorage['recipes'] = JSON.stringify(extendedRecipes);
	
	return recipes;
};