import React, { Component } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

const loaderStyle = {
    width: '150px',
    height: '150px',
    margin: '0 auto',
    display: 'block',
    position: 'absolute',
    top: 'calc(50% - 75px)',
    left: 'calc(50% - 75px)',
};

export const withLoader = (propName) => (WrappedComponent) => {

    return class extends Component {
        isEmpty(prop) {
            return (
                prop === null
                || prop === undefined
                || (Array.isArray(prop) && prop.length === 0)
                || (prop.constructor === Object && Object.keys(prop).length === 0)
            );
        }

       render() {
           return (
               this.isEmpty(this.props[propName])
                       ? <CircularProgress style={loaderStyle} thickness={3} />
                   : <WrappedComponent {...this.props} />
              )
       }
    };
};